import express from "express";
import { engine } from "express-handlebars";
import * as path from "path";
import morgan from "morgan";
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

const app = express();
const port = 3000;
app.use(morgan("combined"));

app.use(express.static(path.join(__dirname, "public")));

app.engine("handlebars", engine());
app.set("view engine", "handlebars");
app.set("views", path.resolve(__dirname, "./resources/views"));

app.get("/", (req, res) => {
  return res.render("home");
});

app.get("/users", (req, res) => {
  return res.render("users");
});

app.listen(port, () => {
  console.log(`Example app listening on port http://localhost:${port}`);
});
